/**
 * Scrape most feeds from http://www.rssfeeds.org
 * and export them as json
 * @see https://github.com/IonicaBizau/scrape-it
 **/

const Fs = require('fs');
const ScrapeIt = require('scrape-it');
const Http = require('http');
const Url = require('url');

const config = {
    // url to scrape
    url: (category, page) => `http://www.rssfeeds.org/rss.go/rss-feeds/${category}.html?page=${page}`,
    // how max feeds to save per category?
    maxFeeds: 500,
    // where to save exported data?
    file: './src/modules/rssFeeds.json',
    // categories to scrap - [url path, title]
    categories: [
        'Arts',
        'Business',
        'Computers',
        'Entertainment',
        'Games',
        'Health',
        'Home',
        'News',
        'Recreation',
        'Regional',
        'Science',
        'Society',
        'Sports',
        'World'
    ],
};

const scrapeTemplates = {
    feed: {
        listItem: 'td[width="640"] > a[target="_blank"]',
        data: {
            title: {
                selector: '',
            },
            url: {
                attr: 'href',
            },
        }
    },
    nextPage: {
        selector: 'a[title="Next Page"]',
        how: ($el) => {
            return parseInt($el[0].attribs.href.match(/[0-9]+$/)[0])
        }
    }
};

const processCategory = (category) => {
    console.log(`BEGIN category: "${category}"`);

    return processPage(category, 1);
};

const processPage = (category, page, allFeeds = []) => {
    return new Promise((resolve, reject) => {
        ScrapeIt(config.url(category, page), {
            feeds: scrapeTemplates.feed,
            nextPage: scrapeTemplates.nextPage,
        })
            .then(({ nextPage, feeds }) => {
                // console.log(`PAGE ${page}  category: "${category}"  feeds: ${feeds.length}`);
                allFeeds.push(...feeds);

                // continue to next page or stop ?
                if (nextPage > page && allFeeds.length <= config.maxFeeds) {
                    resolve(processPage(category, page + 1, allFeeds));
                } else {
                    // add category to every feed and resolve and limit feeds count
                    const limitedFeeds = allFeeds
                        .map((feed) => Object.assign(feed, { category }))
                        .slice(0, config.maxFeeds);

                    // category is done
                    console.log(`DONE category: "${category}" savedFeeds: ${limitedFeeds.length}/${allFeeds.length}  pages: ${page}`);

                    resolve(limitedFeeds);
                }
            })
            .catch(error => {
                console.error(`ERROR category: "${category}"  error: ${error.message}`);
                reject([]);
            });
    });
};

const pingUrl = (url) => {
    return new Promise((resolve, reject) => {
        const request = Http.request({
            method: 'HEAD',
            host: Url.parse(url).host,
            port: 80,
            path: Url.parse(url).pathname,
        }, (response) => {
            const statusOk = response.statusCode >= 200 && response.statusCode < 400;
            statusOk ? resolve(response.statusCode) : reject(response.statusCode);
        });
        request.on('socket', function (socket) {
            socket.setTimeout(5000);
            socket.on('timeout', function () {
                request.abort();
            });
        });
        request.on('error', reject).end();
    });
};

const outputJson = (feeds) => {
    console.log(`FEEDS ONLINE: ${feeds.good.length}/${feeds.all.length}`);

    Fs.writeFile(config.file, JSON.stringify(feeds.good, null, 2), (error) => {
        if (error) throw error;
        console.log(`SUCCESS json file saved to "${config.file}"`);
    });
};

/**
 * Start processing
 **/

Promise.all(config.categories.map(processCategory))
    .then(arrayOfFeeds => {
        const pingPromises = [];
        const feeds = {
            all: [].concat(...arrayOfFeeds),
            good: [],
            bad: []
        };

        // Ping rss feeds urls
        feeds.all.forEach((feed, index) => {
            pingPromises.push(
                pingUrl(feed.url)
                    .then((status) => {
                        // console.log(`PING OK ${status}  ${feed.url}`);
                        feeds.good.push(feed);
                    })
                    .catch((status) => {
                        console.log(`PING ERROR ${status}  ${feed.url}`);
                        feeds.bad.push(feed);
                    })
            );
        });

        Promise.all(pingPromises)
            .then(() => outputJson(feeds))
            .catch(() => outputJson(feeds));
    });
