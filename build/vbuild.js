/**
 * vbuild config
 * @see https://vbuild.js.org
 **/

const Path = require('path');
const Package = require('../package.json');
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');
const Polyfills = require('./polyfills');
const Loader = require('./loader');

const config = {
    eslintConfig: require('../.eslintrc'),

    postcss: [],
    autoprefixer: {
        browsers: ['> 5%'],
    },

    entry: 'src/index.js',
    webpack(config) {
        config.resolve.modules.push(Path.resolve('src'));
        config.plugins.push(
            new FriendlyErrorsWebpackPlugin({
                clearConsole: true,
            })
        );
        return config;
    },

    define: {
        _WEBPACK_: {
            appTitle: JSON.stringify(Package.title),
            version: JSON.stringify(Package.version),
        },
    },

    html: {
        title: Package.title,
        description: Package.description,
        template: './src/index.ejs',
        // custom vars passed to template
        scripts: [
            Polyfills.scriptUrl(),
        ],
        stylesheets: [
            'https://fonts.googleapis.com/css?family=Merriweather:300,400,700',
        ],
        styles: [
            Loader.style,
        ],
        loader: Loader,
    }
}

module.exports = options => config;
