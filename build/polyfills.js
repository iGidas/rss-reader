/**
 * Polyfill ES6 features
 * @see https://polyfill.io/v2/docs/
 **/

/**
 * App features
 * ensure loading of features used in app
 * @type {[string]}
 */

const appFeatures = [
    // Load service defaults for old browsers
    'default-3.6',

    // ES6 Array
    'Array.prototype.includes',
    'Array.prototype.findIndex',
    'Array.prototype.find',
    // ES6 Object
    'Object.assign',
    'Object.keys',
    'Object.entries',
    // ES6 Promise
    'Promise',
    // ES6 Map
    'Map',
];

/**
 * Generates polyfills script url
 * @param {boolean} minify
 * @param {Array} features
 * @return {string}
 */
module.exports = {
    scriptUrl: (minify = true, features = appFeatures) => {
        return [
            `https://polyfill.io/v2/polyfill`,
            `${minify ? '.min' : ''}.js`,
            `?features=`,
            encodeURIComponent(features.join(','))
        ].join('');
    }
};
