/**
 * Inlines SVG loader with CSS rotation
 **/

const Postcss = require('postcss');
const Autoprefixer = require('autoprefixer');
const Fs = require('fs');
const Path = require('path');
const Config = require('./vbuild');

// Load SVG content
module.exports.svg = Fs.readFileSync(
    Path.resolve(__dirname, '../src/icons/loading.svg')
).toString();

// Process CSS
const cssProcessor = Postcss().use(
    Autoprefixer(Config().autoprefixer)
);

module.exports.style = cssProcessor.process(`
    @keyframes spin {
        from { transform: rotate(0turn) }
        to   { transform: rotate(1turn) }
    }
    
    .AppLoader {
        position: fixed;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        z-index: 0;
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        background: #41b883;
    }
    
    .AppLoader > svg {
        height: 200px;
        width: 200px;
        fill: #fff;
        animation: spin 1s infinite linear;
    }
`).css;

// Html
module.exports.html = `
    <div class="AppLoader">
        ${ module.exports.svg }
    </div>
`;
