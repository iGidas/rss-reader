// http://eslint.org/docs/user-guide/configuring

module.exports = {
    root: true,
    parser: 'babel-eslint',
    parserOptions: {
        sourceType: 'module'
    },
    env: {
        browser: true,
    },
    globals: {
        '_WEBPACK_': false,
    },
    extends: 'airbnb-base',
    // required to lint *.vue files
    plugins: [
        'html'
    ],
    // add your custom rules here
    rules: {
        // don't require .vue extension when importing
        'import/extensions': ['error', 'always', {
            'js': 'never',
            'vue': 'never'
        }],

        // allow debugger during development
        'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0,

        // use same setting as .editorconfig
        'indent': ['error', 4],

        // other overrides
        'global-require': ['off'],
        'comma-dangle': ['error', 'always-multiline'],
        'no-param-reassign': ['off'],
        'import/prefer-default-export': ['off'],
    }
};
