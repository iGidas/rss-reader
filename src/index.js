/**
 * Client code entry point
 **/

import Vue from 'vue';

// Load styles
import 'normalize.css';
import './scss/app.scss';

// Load dependencies
import { Store, initStore } from './store';
import { Router, initRouter } from './router';
import { $state, $setArray, $lang } from './modules/vueUtils';
import { initComponents } from './components';

// Init modules
initStore(Vue);
initComponents(Vue);
initRouter(Vue);

// Register mixins
Vue.mixin($state);
Vue.mixin($setArray);
Vue.mixin($lang);

// Create Vue root instance
export const VueRoot = new Vue({
    el: '#app',
    store: Store,
    router: Router,
    render: h => h('app'),
});

// Expose as window global
window.VueRoot = VueRoot;
