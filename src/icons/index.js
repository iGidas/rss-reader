/**
 * Imports all icons and saves their html to JS
 */

import { importAll } from '../modules/file';

export const icons = {};

// create Webpack require context
const IconsReq = require.context('!raw-loader!../icons/', true, /\.svg$/);

importAll(IconsReq, (file, module) => {
    // cache icons
    icons[file.name] = module;
});
