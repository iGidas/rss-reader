export default {
    lbl: {
        signUp: 'Sign Up',
        logIn: 'Log In',
        logOut: 'Log Out',
    },

    welcome: {
        firstTime: {
            hi: 'Welcome to {appTitle}!',
            name: {
                empty: 'What should we call you?',
                entered: 'Nice to meet you, {name}!',
                placehold: 'Name',
            },
        },
        returning: {
            hi: 'Welcome back to {appTitle}!',
            name: 'Please enter your log in name',
        },
    },

    favorites: {
        add: 'Add to favorites',
        remove: 'Remove from favorites',
    },

    history: {
        remove: 'Remove from history',
    },

    search: {
        queryParam: 'query',
        label: 'Enter feed url:',
        submit: 'Load feed',
        placehold: 'http://...',
    },
};
