/**
 * Imports all translations
 */

import { importAll } from '../modules/file';

export const languages = {};

// create Webpack require context
const ModulesReq = require.context('./', true, /lang-\w+\.(js|json)/);

importAll(ModulesReq, (file, module) => {
    const name = file.name.replace('lang-', '');
    languages[name] = module;
});
