/**
 * Parse path parts
 * @param path ex.: 'some/temp/path/fileName.ext'
 * @returns {{path: array, name: string, ext: string}}
 */
export const parsePath = (path) => {
    const parts = path.split('/');
    const file = parts.pop().split('.');
    return {
        path: parts,
        name: file[0],
        ext: file[1],
    };
};

/**
 * Imports all files from folder (Webpack helper)
 * @see http://devdocs.io/webpack~2/guides/dependency-management/index
 * @param req
 * @param callback
 */
export const importAll = (req, callback) => {
    req.keys().forEach((path) => {
        let module = req(path);

        // If ES6 module
        // eslint-disable-next-line no-underscore-dangle
        if (module.__esModule && module.default) {
            module = module.default;
        }

        if (callback) {
            callback.call(this, parsePath(path), module);
        }
    });
};
