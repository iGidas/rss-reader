/**
 * Converts image to data uri
 * Based on: https://github.com/henrikjoreteg/image-to-data-uri.js
 * Converted to ES6 and promises
 **/

export const canvasToBase64 = (img) => {
    const canvas = document.createElement('canvas');

    if (!canvas.getContext) {
        throw new Error('canvasToBase64', '<canvas> is not supported');
    }

    const ctx = canvas.getContext('2d');

    // Match size of image
    canvas.width = img.naturalWidth;
    canvas.height = img.naturalHeight;

    // Copy the image contents to the canvas
    ctx.drawImage(img, 0, 0);

    // Get the data-URI formatted image
    return canvas.toDataURL('image/png');
};

export const imageToBase64 = (image) => {
    const isNode = image instanceof window.HTMLImageElement;
    const img = isNode ? image : document.createElement('img');
    img.setAttribute('crossOrigin', 'anonymous');

    return new Promise((resolve, reject) => {
        if (isNode) {
            resolve(canvasToBase64(img));
        } else {
            img.addEventListener('error', reject);
            img.addEventListener('load', () => resolve(canvasToBase64(img)));
            img.src = image;
        }
    });
};

window.imageToBase64 = imageToBase64;
