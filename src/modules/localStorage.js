/**
 * Class to interact with browser localStorage
 **/

import { promisify, tryify, noop } from './utils';

class LocalStorageClass {
    constructor() {
        tryify(() => {
            this.store = window.localStorage;
        });
    }

    set(key, value) {
        return promisify(() => {
            LocalStorageClass.log('set', key);
            this.store.setItem(key, JSON.stringify({ value }));
        }).catch(noop);
    }

    get(key, defaultValue) {
        LocalStorageClass.log('get', key);
        const value = tryify(() => JSON.parse(this.store.getItem(key)).value);
        return value || defaultValue;
    }

    remove(key) {
        return promisify(() => {
            this.store.removeItem(key);
        });
    }

    static log(action, key) {
        // eslint-disable-next-line no-console
        console.log('localStorage', action, key);
    }
}

export const LocalStorage = new LocalStorageClass();
