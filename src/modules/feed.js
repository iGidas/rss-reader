/**
 * Feed class
 * all methods are static,
 * because Vue removes all simple class methods
 * then observes object
 **/

import { commit, state, getter } from '../store';
import { imageToBase64 } from './base64';

export class Feed {
    constructor(feed) {
        // Custom properties
        this.id = '';
        this.image64 = '';
        this.isIn = {
            favorites: false,
            history: false,
        };

        // From api
        this.feed = {};
        this.items = [];

        // Merge with defaults
        Object.assign(this, feed);

        // Init
        if (!this.id) {
            this.id = this.feed.url;
        }
        if (this.image64 !== false) {
            Feed.tryCacheImage.apply(this);
        }
    }

    static isStored() {
        return Feed.getIndex.apply(this) > -1;
    }

    static getIndex() {
        return state('feeds').feeds.findIndex(feed => feed.id === this.id);
    }

    static update(update = {}) {
        const newFeed = Object.assign({}, getter('feeds', 'getById')(this.id), update);
        commit('feeds/UPDATE_FEED', newFeed);
    }

    static tryCacheImage() {
        imageToBase64(this.feed.image)
            .then(image64 => Feed.update.call(this, {
                image64,
            }))
            .catch(() => Feed.update.call(this, {
                image64: false,
            }));
    }
}
