/**
 * Ajax utility based on VueResource
 * @see https://github.com/pagekit/vue-resource/blob/develop/docs
 **/

import Vue from 'vue';
import VueResource from 'vue-resource';

// Register Vue plugin
Vue.use(VueResource);

// Create empty Vue instance, to access VueResource methods
export const Ajax = new Vue();
