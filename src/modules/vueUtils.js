import _ from './lodash';

/**
 * Vue mixins
 * @see https://vuejs.org/v2/guide/mixins
 **/

// Expose current state (only getter)
export const $state = {
    computed: {
        $state: {
            get() {
                return this.$store.state;
            },
        },
    },
};

// Easily update property of array item
export const $setArray = {
    methods: {
        $setArray: (array, item, path, value) => {
            const index = array.indexOf(item);
            this.$set(array, index, _.merge(
                item,
                _.set({}, path, value),
            ));
        },
    },
};

// Wrap $t translation method and pass default properties to it
export const $lang = {
    computed: {
        $lang() {
            return (translation, data) => this.$t(translation, data || this);
        },
    },
};
