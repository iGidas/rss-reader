/**
 * Do nothing
 **/

export function noop() {
}

/**
 * Utility to run function in promise
 **/

export const promisify = func => new Promise((resolve) => {
    resolve(func());
});

/**
 * Easier try-catch
 **/

/* eslint-disable consistent-return */
export const tryify = (func) => {
    try {
        return func();
    } catch (error) {
        noop(error);
    }
};
/* eslint-enable consistent-return */
