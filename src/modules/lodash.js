/**
 * Import only specific lodash functions for smaller build size
 */

export default {
    isFunction: require('lodash.isfunction'),
    kebabCase: require('lodash.kebabcase'),
    cloneDeep: require('lodash.clonedeep'),
    sortBy: require('lodash.sortby'),
    set: require('lodash.set'),
    merge: require('lodash.merge'),
};
