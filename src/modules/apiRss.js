/**
 * Module to communicate with rss2json api
 * @see https://rss2json.com/docs
 **/

import { Ajax } from './ajax';

/**
 * Normalizes response
 * @param commit
 * @param response
 * @return {*}
 */
const handleResponse = (response) => {
    // Check if error happen in client
    if (!response.ok) {
        throw new Error('Failed to get feed data, try again later');
    }
    // Check if error happen in api
    if (response.body.status === 'error') {
        throw new Error(`API: ${response.body.message}`);
    }
    return response.body;
};

/**
 * Create api caller
 * @param params
 * @return {Promise}
 */
export const request = (params) => {
    const promise = Ajax.$http.get('https://api.rss2json.com/v1/api.json', {
        // Merge parameters
        params: Object.assign({
            rss_url: '',            // escaped url
            api_key: 'vt0nrzlubigh3i6pordm473c2e89c02ntquueubl',
            order_by: 'pubDate',    // 'pubDate', 'author', 'title'
            order_dir: 'asc',       // 'asc', 'desc'
            count: 10,
            timeout: 10,
        }, params),
    });

    // preprocess response
    return promise.then(handleResponse, handleResponse);
};
