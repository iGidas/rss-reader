/**
 * Global router hooks and guards
 **/

import { commit, getter } from '../store';

/**
 * Validate what routes user can reach
 * @param args
 * @param {function} callback
 */

function authorize({ to, from }, callback) {
    // Check if next route is allowed
    const authorized = to.meta.accessLevel.includes(getter('user', 'level'));

    // Redirect by user level
    const redirect = ({
        guest: 'welcome',
        loggedIn: 'homepage',
    })[getter('user', 'level')];

    callback(authorized, redirect);
}

/**
 *  Init Router hooks
 * @param Router
 * @param routes
 */
export const init = (Router, routes) => {
    // Init stores
    commit('user/NEW_BROWSER_SESSION');
    commit('app/SET_STATE', { routes });

    // Guard
    Router.beforeEach((to, from, next) => {
        authorize({ to, from }, (authorized, redirectTo) => {
            // eslint-disable-next-line no-console
            console.log({
                level: getter('user', 'level'),
                authorized,
                redirectTo,
            });

            if (authorized) {
                next();

                // Log session every time user navigates
                if (getter('user', 'level') === 'loggedIn') {
                    commit('user/LOG_USER_SESSION');
                }
            } else {
                next({ name: redirectTo });
            }
        });
    });

    // Hook
    Router.afterEach((to) => {
        commit('app/SET_PAGE_TITLE', to.meta.pageTitle);
        commit('app/SET_STATE', { currentRoute: to });
    });
};

