/**
 * Initiation of app router
 * @see https://router.vuejs.org/en/
 **/

import Vue from 'vue';
import VueRouter from 'vue-router';
import { routes } from './routes';
import { init } from './hooks';

// Register Vue plugin
Vue.use(VueRouter);

// Create Router instance
export const Router = new VueRouter({
    mode: 'history',
    routes,
});

export const initRouter = () => {
    // Init global hooks
    init(Router, routes);
};

// Reexport
export { routes } from './routes';
