/**
 * All app routes
 **/

/* eslint-disable import/no-unresolved */
export const routes = [
    // Route entry point
    {
        path: '/',
        redirect: {
            name: 'welcome',
        },
    },
    {
        name: 'welcome',
        path: '/welcome',
        component: require('../components/route-welcome'),
        meta: {
            pageTitle: 'Welcome',
            accessLevel: ['guest'],
        },
    },
    {
        name: 'homepage',
        path: '/app',
        component: require('../components/route-homepage'),
        meta: {
            pageTitle: 'Homepage',
            accessLevel: ['loggedIn'],
        },
    },
    {
        name: 'history',
        path: '/app/history',
        component: require('../components/route-history'),
        meta: {
            pageTitle: 'History',
            accessLevel: ['loggedIn'],
        },
    },
    {
        name: 'favorites',
        path: '/app/favorites',
        component: require('../components/route-favorites'),
        meta: {
            pageTitle: 'Favorites',
            accessLevel: ['loggedIn'],
        },
    },
    {
        name: 'feed',
        path: '/app/feed/:index?',
        component: require('../components/route-feed'),
        meta: {
            pageTitle: 'Feed',
            accessLevel: ['loggedIn'],
        },
    },
    {
        name: 'search',
        path: '/app/search',
        component: require('../components/route-search'),
        meta: {
            pageTitle: 'Search',
            accessLevel: ['loggedIn'],
        },
    },
    // 404 redirects to index
    {
        name: '404',
        path: '*',
        redirect: '/',
    },
];
