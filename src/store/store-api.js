/**
 * Store module to communicate with rss2json api
 **/

import { request } from '../modules/apiRss';

const apiRequest = ({ params, commit, resolve }) => {
    request(params)
        // success
        .then((response) => {
            commit('SET_STATE', {
                isInProgress: false,
                isDone: true,
                isOk: true,
                response,
            });
            resolve({
                feed: response.feed,
                items: response.items,
            });
        })
        // error
        .catch((error) => {
            commit('SET_STATE', {
                isInProgress: false,
                isDone: true,
                isError: true,
                error: error.message,
            });
        });
};

export default {
    state: {
        response: {},
        error: '',
        isInProgress: false,
        isDone: false,
        isError: false,
        isOk: false,
    },

    mutations: {
        SET_STATE(state, payload) {
            Object.assign(state, payload);
        },
    },

    actions: {
        GET_FEED({ commit }, params = {}) {
            // Before ajax
            commit('SET_STATE', {
                response: {},
                error: '',
                isInProgress: true,
                isDone: false,
                isError: false,
                isOk: false,
            });
            // Make ajax
            return new Promise(resolve => apiRequest({ params, commit, resolve }));
        },
    },
};
