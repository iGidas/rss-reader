/**
 * Initiation of app Vuex store
 * @see https://vuex.vuejs.org/en/
 **/

import Vue from 'vue';
import Vuex from 'vuex';
import { importAll } from '../modules/file';

const ModulesReq = require.context('./', true, /store-\w+\.js/);

// Register Vue plugin
Vue.use(Vuex);

// Create Vuex instance
export const Store = new Vuex.Store({
    strict: true,
});

// Helpers
export const commit = Store.commit;
export const dispatch = Store.dispatch;
export const state = module => Store.state[module];
export const getter = (module, key) => Store.getters[`${module}/${key}`];


// Init store
export const initStore = (vue) => {
    // Import and register all store modules
    importAll(ModulesReq, (file, module) => {
        const name = file.name.replace('store-', '');

        Store.registerModule(name, {
            namespaced: true,
            ...module,
        });

        // Call custom 'init' method
        // eslint-disable-next-line no-unused-expressions
        module.init && module.init(Store, vue);
    });
};
