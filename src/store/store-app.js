/**
 * Store module for app basics
 */

export default {
    state: {
        version: _WEBPACK_.version,
        appTitle: _WEBPACK_.appTitle,
        pageTitle: '',
        classes: [],

        routes: [],
        currentRoute: {},
    },

    getters: {
        route: state => name => state.routes.find(
            route => route.name === name
        ),
    },

    mutations: {
        SET_PAGE_TITLE(state, title) {
            state.pageTitle = title;
            document.title = `${state.appTitle} | ${title}`;
        },

        SET_STATE(state, payload) {
            Object.assign(state, payload);
        },

        TOGGLE_CLASSES({ classes }, payload) {
            payload.classes.forEach((className) => {
                // Add
                if (payload.state && !classes.includes(className)) {
                    classes.push(className);
                }
                // Remove
                if (!payload.state && classes.includes(className)) {
                    classes.splice(classes.indexOf(className), 1);
                }
            });
        },
    },
};
