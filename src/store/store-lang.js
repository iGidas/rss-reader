/**
 * Translate Vue components using vuex-i18n
 * @see https://github.com/dkfbasel/vuex-i18n
 **/

import VuexI18n from 'vuex-i18n';
import { languages } from '../lang';

// Export store
export default {
    init: (Store, Vue) => {
        Vue.use(VuexI18n.plugin, Store, 'lang');

        // Add languages
        Object.entries(languages).forEach(([locale, translations]) => {
            Store.commit('ADD_LOCALE', { locale, translations });
        });

        // Set starting locales
        Store.commit('SET_LOCALE', { locale: 'en' });
        Store.commit('SET_FALLBACK_LOCALE', { locale: 'en' });
    },

    namespaced: false,
    ...VuexI18n.store,
};
