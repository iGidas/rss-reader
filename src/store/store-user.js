/**
 * Store module to save info about user
 **/

import { LocalStorage } from '../modules/localStorage';

// Helper - convert array of ISO date strings into date objects
const convertDates = dates => dates.map(date => new Date(date));

// Load data
const loadUserData = (name) => {
    const user = LocalStorage.get(`user/data/${name}`, {
        name,
        sessions: [],
    });
    user.sessions = convertDates(user.sessions);
    return user;
};

const loadBrowserData = () => {
    const browser = LocalStorage.get('user/browser', {
        sessions: [],
    });
    browser.sessions = convertDates(browser.sessions);
    return browser;
};

const loadCurrentUser = () => LocalStorage.get('user/current');

// Set date
const saveUserData = user => LocalStorage.set(`user/data/${user.name}`, user);
const saveCurrentUser = user => LocalStorage.set('user/current', user.name);
const saveBrowserData = browser => LocalStorage.set('user/browser', browser);

export default {
    init: (Store) => {
        // Auto login
        const name = loadCurrentUser();
        // eslint-disable-next-line no-unused-expressions
        name && Store.dispatch('user/LOG_IN', { name });
    },

    state: {
        user: {},
        browser: loadBrowserData(),
    },

    getters: {
        isFirstVisit: ({ browser }) => (browser.sessions.length <= 1),
        level: ({ user }) => (user.name ? 'loggedIn' : 'guest'),
    },

    mutations: {
        SET_STATE(state, payload) {
            Object.assign(state, payload);
        },

        // Push new user session
        NEW_USER_SESSION({ user }) {
            user.sessions.push(Date());
            saveUserData(user);
        },

        // Push new browser session
        NEW_BROWSER_SESSION({ browser }) {
            browser.sessions.push(new Date());
            saveBrowserData(browser);
        },

        // Update last user session date
        LOG_USER_SESSION({ user }) {
            user.sessions.splice(-1, 1, new Date());
            saveUserData(user);
        },
    },

    actions: {
        LOG_IN: ({ state, commit, dispatch }, { name }) => new Promise((resolve) => {
            commit('SET_STATE', {
                user: loadUserData(name),
            });
            commit('NEW_USER_SESSION');
            saveCurrentUser(state.user);
            dispatch('feeds/LOAD_FEEDS', null, { root: true });
            resolve();
        }),

        LOG_OUT: ({ state, commit, dispatch }) => new Promise((resolve) => {
            commit('LOG_USER_SESSION');
            commit('SET_STATE', {
                user: {},
            });
            saveCurrentUser(state.user);
            dispatch('feeds/CLEAR_FEEDS', null, { root: true });
            resolve();
        }),
    },
};
