/**
 * Imports all Vue components
 */

import { importAll } from '../modules/file';

// create Webpack require context
const ComponentsReq = require.context('components/', true, /\.(vue|js)$/);

export const initComponents = (Vue) => {
    importAll(ComponentsReq, (file, module) => {
        // register component globally
        Vue.component(file.name, module);
    });
};
